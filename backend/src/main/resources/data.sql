INSERT INTO users
(id, username, avatar_link)
VALUES
('9e243930-83c9-11e9-8e0c-8f1a686f4ce4', 'Ruth', 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA'),
('533b5230-1b8f-11e8-9629-c7eca82aa7bd', 'Wendy', 'https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng'),
('4b003c20-1b8f-11e8-9629-c7eca82aa7bd', 'Helen', 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ'),
('5328dba1-1b8f-11e8-9629-c7eca82aa7bd', 'Ben', 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg');


INSERT INTO messages
(id, user_id, body, created_at, edited_at)
VALUES
(
    '80f08600-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'I don’t *** understand. It’s the Panama accounts',
    '2020-06-16 19:48:12.936',
    null
), (
    '80e00b40-1b8f-11e8-9629-c7eca82aa7bd',
    '533b5230-1b8f-11e8-9629-c7eca82aa7bd',
    'Tells exactly what happened.',
    '2020-06-16 19:48:42.481',
    '2020-07-16 19:48:47.481'
), (
    '80e03259-1b8f-11e8-9629-c7eca82aa7bd',
    '533b5230-1b8f-11e8-9629-c7eca82aa7bd',
    'You were doing your daily bank transfers and…',
    '2020-06-16 19:48:56.273',
    null
), (
    '80e03258-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'Yes, like I’ve been doing every *** day without red *** flag',
    '2020-07-16 19:49:14.480',
    null
), (
    '80e03257-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'There’s never been a *** problem.',
    '2020-07-16 19:48:28.769',
    null
), (
    '80e03256-1b8f-11e8-9629-c7eca82aa7bd',
    '4b003c20-1b8f-11e8-9629-c7eca82aa7bd',
    'Why this account?',
    '2020-07-16 19:49:33.195',
    null
), (
    '80e03255-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'I don’t *** know! I don’t know!',
    '2020-07-16 19:49:45.821',
    null
), (
    '80e03254-1b8f-11e8-9629-c7eca82aa7bd',
    '5328dba1-1b8f-11e8-9629-c7eca82aa7bd',
    'What the ** is a red flag anyway?',
    '2020-07-16 19:50:07.708',
    null
), (
    '80e03253-1b8f-11e8-9629-c7eca82aa7bd',
    '4b003c20-1b8f-11e8-9629-c7eca82aa7bd',
    'You said you could handle things',
    '2020-07-16 19:53:02.483',
    null
), (
    '80e03252-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'I did what he taught me.',
    '2020-07-17 18:53:17.272',
    '2020-07-16 19:53:50.272'
), (
    '80e03251-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'it’s not my fucking fault!',
    '2020-07-18 19:53:49.171',
    null
), (
    '80e03250-1b8f-11e8-9629-c7eca82aa7bd',
    '533b5230-1b8f-11e8-9629-c7eca82aa7bd',
    'Can you fix this? Can you fix it?',
    '2020-07-19 19:56:51.491',
    null
), (
    '80e03249-1b8f-11e8-9629-c7eca82aa7bd',
    '4b003c20-1b8f-11e8-9629-c7eca82aa7bd',
    'Her best is gonna get us all killed.',
    '2020-07-19 19:57:07.965',
    '2020-07-19 19:57:15.965'
), (
    '80e03248-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'I don’t know how!',
    '2020-07-19 19:58:06.686',
    null
), (
    '80e03247-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'it means that the accounts frozen that cause the feds might think that there’s a crime being committed.',
    '2020-07-20 19:52:04.375',
    null
), (
    '80e03246-1b8f-11e8-9629-c7eca82aa7bd',
    '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    'Like by me',
    '2020-07-21 19:52:15.334',
    null
), (
    '80e03245-1b8f-11e8-9629-c7eca82aa7bd',
    '5328dba1-1b8f-11e8-9629-c7eca82aa7bd',
    'aaaha’Q!',
    '2020-07-21 19:58:17.878',
    null
);

